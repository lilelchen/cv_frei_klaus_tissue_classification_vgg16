# CV_Frei_Klaus_Tissue_Classification_VGG16

**Project Title:** Tissue Classification VGG16

**Program:** MSc. Applied Information & Data Science

**Supervisor:** Dr. Umberto Michelucci & Dr. Mirko Birbaumer

**Authors:** Manuel Frei & Anna-Lena Klaus

**Date:** 30.03.2023



In this project, eight tissue classes get classified by an optimized VGG16 model. To run the code, clone the project to your folder and adjust the paths within the three notebooks, that can be found in the following repository structure:



```

├── VGG16
    ├── code
        └── CV_data_modelling_frei_klaus_VGG16_30_03.ipynb
        └── CV_data_preparation_frei_klaus_VGG16_30_03.ipynb
        └── CV_data_validation_frei_klaus_VGG16_30_03.ipynb
    └── data
        └── HTC_28_28_RGB
        └── Kather_texture_2016_image_tiles_5000
        └── preprocessed_data/tissue_classification
    └── logs/hparam_tuning
    └── models_weights
```

